<?php
/*
Plugin Name: GF Plugins
Plugin URI:
Description: Green friends shop theme plugins
Author: Green Friends
Author URI: http://greenfriends.systems/
Version: 1.0.0
*/

namespace GfWpPluginContainer;

use GfWpPluginContainer\Wp\WpEnqueue;
use GfWpPluginContainer\Wp\ShortCodes;
use GfWpPluginContainer\Wp\Widgets;

define('PLUGIN_DIR_URI', plugin_dir_url(__DIR__ . '/gfShopThemePlugins/'));
define('PLUGIN_DIR', __DIR__ . '/');


/**
 * Trenutno će se settings pagevi konfigurisati kroz array, u budučnosti možemo i napraviti core clasu
 * al za sada mislim da je ovo bolje zbog perfomansi pošto je kod za kreiranje settinga za plugin isti pa ne
 * vidim poentu pravljenja posebnih klasa
 *
 */
class GfShopThemePlugins
{

    public function __construct()
    {
        $this->init();
    }

    private function actionAdminMenu($function)
    {
        add_action('admin_menu', function () use ($function) {
            $function();
        });
    }

    private function init()
    {
        WpEnqueue::addGlobalAdminStyle('bootstrap_css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
        WpEnqueue::actionAdminEnqueueScripts(static function () {
            wp_enqueue_media();
        });
        WpEnqueue::addFrontendScript('jqueryUi', 'https://code.jquery.com/jquery-1.12.4.js');
        WpEnqueue::addFrontendScript('jqueryUiMin', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js');
        $this->addMainSettingPage();
        $this->setupPlugins();
        $this->actionAdminMenu(function (){
            remove_submenu_page('theme_plugins','theme_plugins');
        });
        $wooSetup = new WooSetup();

        add_action('plugins_loaded', function () use ($wooSetup){
            $wooSetup->initClasses();
        });
        $widgets = new Widgets();
        $widgets->registerWidgets();
    }

    private function setupPlugins()
    {
        foreach (glob(__DIR__ . '/config/plugins/*.php') as $file) {
            $pluginInfo = require $file;
            $adminTemplateDirUri = PLUGIN_DIR_URI . 'templates/admin/';
            $scTemplateDir = PLUGIN_DIR . 'templates/shortcodes';
            $scTemplateDirUri = PLUGIN_DIR_URI . 'templates/shortcodes';
            $pageTitle = __($pluginInfo['settingPage']['pageTitle'],'gfShopThemePlugins');
            $menuTitle = $pluginInfo['settingPage']['menuTitle'];
            $capability = $pluginInfo['settingPage']['capability'];
            $menuSlug = $pluginInfo['settingPage']['menuSlug'];
            $templateName = $pluginInfo['settingPage']['template'];
            $optionGroup = $pluginInfo['registerSettings']['optionGroup'];

            if (strlen($pageTitle) > 0) {
                foreach ($pluginInfo['registerSettings']['options'] as $optionName => $optionArgs) {
                    $this->registerSetting($optionGroup, $optionName, $optionArgs);
                }
                $this->addSubmenuPage($pageTitle, $menuTitle, $capability, $menuSlug, $templateName);
                WpEnqueue::addAdminStyle($templateName, $menuSlug, $adminTemplateDirUri . $templateName . '/css/' . $templateName . '.css', ['bootstrap_css']);
                WpEnqueue::addAdminScript($templateName, $menuSlug, $adminTemplateDirUri . $templateName . '/js/' . $templateName . '.js');
            }
            if (isset($pluginInfo['shortCode'])){
                ShortCodes::setupShortCode($scTemplateDir, $scTemplateDirUri, $pluginInfo['shortCode']);
            }
        }
    }

    private function addSubmenuPage($pageTitle, $menuTitle, $capability, $menuSlug, $templateName)
    {
        $this->actionAdminMenu(function () use ($pageTitle, $menuTitle, $capability, $menuSlug, $templateName) {
            add_submenu_page('theme_plugins', $pageTitle, $menuTitle, $capability, $menuSlug, function () use ($templateName) {
                $this->getTemplatePart('admin', $templateName);
            });
        });

        return $this;
    }

    /*
     * Page is empty because its not used its only used to create header menu for settings
     */
    private function addMainSettingPage()
    {
        $this->actionAdminMenu(function () {
            add_menu_page('Theme plugin settings', 'Theme plugin settings', 'manage_options', 'theme_plugins', null,null);
        });
    }

    public function getTemplatePart($section, $name)
    {
        require_once PLUGIN_DIR . '/templates/' . $section . '/' . $name . '/view/' . $name . '.php';
    }

    public static function getTemplatePartials($section, $pluginName, $fileName, $data = [])
    {
        require PLUGIN_DIR . '/templates/' . $section . '/' . $pluginName . '/view/' . $fileName . '.php';
    }

    private function registerSetting($optionGroup, $optionName, $optionArgs)
    {
        $this->actionAdminMenu(function () use ($optionGroup, $optionName, $optionArgs) {
            if (count($optionArgs) > 0) {
                register_setting($optionGroup, $optionName, $optionArgs);
            }
            register_setting($optionGroup, $optionName);
        });
    }


}

new GfShopThemePlugins();