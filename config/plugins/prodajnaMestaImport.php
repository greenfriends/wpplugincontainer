<?php
return [
    'settingPage' => [
        'pageTitle' => 'Import prodajnih mesta',
        'menuTitle' => 'Prodajna mesta import',
        'capability' => 'manage_options',
        'menuSlug' => 'prodajna-mesta-import',
        'template' => 'prodajnaMestaImport'
    ],
    'registerSettings' => [
        'optionGroup' => 'prodjnaMestaOptionGroup',
        'options' => [
            'prodajnaMestaImport' => []
        ]
    ]
];