<?php


namespace GfWpPluginContainer\Widgets;


class CounterItem extends \WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'gf_counter_item',
            'Counter Item',
            ['description' => 'Widget for displaying counter items']
        );

    }

    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        $image = '';
        $imageTwo = '';
        $itemText = '';
        $counterNumber = '';
        if (isset($instance['image']) && $instance['image'] !== '') {
            $image = $instance['image'];
        }
        if (isset($instance['itemText']) && $instance['itemText'] !== '') {
            $itemText = $instance['itemText'];
        }
        if (isset($instance['counterNumber']) && $instance['counterNumber'] !== '') {
            $counterNumber = $instance['counterNumber'];
        }



        ?>
        <label for="<?= $this->get_field_id('image') ?>">Izaberite sliku</label>
        <div>
            <input type="hidden" id="<?= $this->get_field_id('image') ?>" class="image-upload"
                   name="<?= $this->get_field_name('image') ?>" value="<?= esc_url($image) ?>">
            <?php
            if (isset($instance['image']) && $instance['image'] !== '') {
                echo '<img src="' . $instance['image'] . '" alt="Counter Image" class="selectedImageHomeItem" style="background:gray;">';
            }
            else {
                echo '<img src="" class="selectedImageHomeItem" style="background:gray; color:white;">';
            }
            ?>
            <button type="button" class="button button-primary js-image-upload">Izaberite sliku</button>
        </div>

        <label for="<?= $this->get_field_id('itemText') ?>">Izaberite tekst ispod slike</label>
        <div>
            <input type="text" id="<?= $this->get_field_id('itemText') ?>"
                   name="<?= $this->get_field_name('itemText') ?>" value="<?= esc_attr($itemText) ?>">
        </div>

        <label for="<?= $this->get_field_id('counterNumber') ?>">Izaberite broj za brojač</label>
        <div>
            <input type="number" id="<?= $this->get_field_id('counterNumber') ?>"
                   name="<?= $this->get_field_name('counterNumber') ?>" value="<?= esc_attr($counterNumber) ?>">
        </div>
        <?php
    }
    /**
     * Front-end display of widget.
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     * @see WP_Widget::widget()
     *
     */

    public function widget($args, $instance)
    {
        if (isset($instance['itemText'], $instance['counterNumber']) && $instance['itemText'] !== '') {
            $html = sprintf('
                <div class="counterItem">
                <div class="counterData">
                <div class="imageContainerCounter">
                    <img src="%s" alt="%s">
                </div>
                <div class="counter-value" data-count="%d">0</div>
                </div>
                <div class="counterText">
                    <p>%s</p>
                </div>
                </div>
            ',$instance['image'], 'Counter Icon', $instance['counterNumber'], $instance['itemText']);
            echo $html;
        }
    }


    /**
     * Sanitize widget form values as they are saved.
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     * @see WP_Widget::update()
     *
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['image'] = (!empty($new_instance['image'])) ? $new_instance['image'] : '';
        $instance['itemText'] = (!empty($new_instance['itemText'])) ? $new_instance['itemText'] : '';
        $instance['counterNumber'] = (!empty($new_instance['counterNumber'])) ? $new_instance['counterNumber'] : '';

        return $instance;
    }
}