<?php


namespace GfWpPluginContainer\Widgets;


class HomepageItem extends \WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'gf_homepage_main',
            'Homepage Item',
            ['description' => 'Widget for displaying linked images with text']
        );

    }


    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        $inputField = '';
        $image = '';
        $itemText = '';
        if (isset($instance['inputField']) && $instance['inputField'] !== '') {
            $inputField = $instance['inputField'];
        }
        if (isset($instance['image']) && $instance['image'] !== '') {
            $image = $instance['image'];
        }
        if (isset($instance['itemText']) && $instance['itemText'] !== '') {
            $itemText = $instance['itemText'];
        }



        ?>
        <label for="<?= $this->get_field_id('inputField') ?>">Unesite putanju</label>
        <div>
            <input type="url" id="<?= $this->get_field_id('inputField') ?>"
                   name="<?= $this->get_field_name('inputField') ?>" value="<?= esc_url($inputField)?>">
        </div>

        <label for="<?= $this->get_field_id('image') ?>">Izaberite sliku</label>
        <div>
            <input type="hidden" id="<?= $this->get_field_id('image') ?>" class="image-upload"
                   name="<?= $this->get_field_name('image') ?>" value="<?= esc_url($image) ?>">
            <?php
            if (isset($instance['image']) && $instance['image'] !== '') {
                echo '<img src="' . $instance['image'] . '" alt="Home Item Image" class="selectedImageHomeItem">';
            }
            else {
                echo '<img src="" alt="Home Item Image" class="selectedImageHomeItem">';
            }
            ?>
            <button type="button" class="button button-primary js-image-upload">Izaberite sliku</button>
        </div>

        <label for="<?= $this->get_field_id('itemText') ?>">Izaberite tekst ispod slike</label>
        <div>
            <input type="text" id="<?= $this->get_field_id('itemText') ?>"
                   name="<?= $this->get_field_name('itemText') ?>" value="<?= esc_attr($itemText) ?>">
        </div>
        <?php
    }

    /**
     * Front-end display of widget.
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     * @see WP_Widget::widget()
     *
     */

    public function widget($args, $instance)
    {
        if (isset($instance['inputField'], $instance['itemText']) && $instance['inputField'] !== '' && $instance['itemText'] !== '') {
            echo '<li><a href="' . $instance['inputField'] .'" title="' . $instance['itemText'] .'">
                 <div class="mainNavImgContainer">
                            <img src="' . $instance['image'] . '"
                                 alt="' . $instance['itemText'] . '">
                        </div>
                        <div class="mainNavTextContainer">
                            <span>' . $instance['itemText'] . '</span>
                        </div>
                        </a>
                        </li>';
        }
    }


    /**
     * Sanitize widget form values as they are saved.
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     * @see WP_Widget::update()
     *
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['inputField'] = (!empty($new_instance['inputField'])) ? $new_instance['inputField'] : '';
        $instance['image'] = (!empty($new_instance['image'])) ? $new_instance['image'] : '';
        $instance['itemText'] = (!empty($new_instance['itemText'])) ? $new_instance['itemText'] : '';

        return $instance;
    }
} 