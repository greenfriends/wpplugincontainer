<?php


namespace GfWpPluginContainer\Widgets;


class WeCareButton extends \WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'gf_weCare_button',
            'WeCare Button',
            ['description' => 'Widget for displaying linked buttons']
        );
    }

    /**
     * Back-end widget form.
     *
     * @param array $instance Previously saved values from database.
     * @see WP_Widget::form()
     *
     */
    public function form($instance)
    {
        $inputField = '';
        $buttonText = '';
        $selectedBlue = '';
        $selectedWhite = '';
        $radio_buttons = '';
        if (isset($instance['inputField']) && $instance['inputField'] !== '') {
            $inputField = $instance['inputField'];
        }
        if (isset($instance['buttonText']) && $instance['buttonText'] !== '') {
            $buttonText = $instance['buttonText'];
        }
        if (isset($instance['radio_buttons']) && $instance['radio_buttons'] !== '') {
            $radio_buttons = $instance['radio_buttons'];
        }



        if (isset($instance['buttonColor'])) {
            switch ($instance['buttonColor']) {
                case 'blueButtonHome':
                    $selectedBlue = 'selected';
                    break;
                case 'whiteButtonHome':
                    $selectedWhite = 'selected';
                    break;
            }
        }
        ?>

        <label for="<?= $this->get_field_id('buttonColor') ?>">Izaberite boju dugmeta</label>
        <div>
            <select id="<?= $this->get_field_id('buttonColor') ?>" name=<?= $this->get_field_name('buttonColor') ?>>
                <option <?= $selectedBlue ?> value="blueButtonHome">Plava</option>
                <option <?= $selectedWhite ?> value="whiteButtonHome">Bela</option>
            </select>
        </div>
        <label for="<?= $this->get_field_id('inputField') ?>">Unesite putanju</label>
        <div>
            <input type="url" id="<?= $this->get_field_id('inputField') ?>"
                   name="<?= $this->get_field_name('inputField') ?>" value="<?= esc_url($inputField)?>">
        </div>
        <label for="<?= $this->get_field_id('buttonText') ?>">Unesite tekst za dugme</label>
        <div>
            <input type="text" id="<?= $this->get_field_id('buttonText') ?>"
                   name="<?= $this->get_field_name('buttonText') ?>" value="<?= esc_attr($buttonText)?>">
        </div>

        <div>
            <label for="<?php echo $this->get_field_id('radio_buttons'); ?>">Otvorite link u novom tab-u </label>
                <input class="" id="<?php echo $this->get_field_id('radio_option_1'); ?>" name="<?php echo $this->get_field_name('radio_buttons'); ?>" type="radio" value="newTab" <?php if($radio_buttons === 'newTab'){ echo 'checked="checked"'; } ?> />

            <label for="<?php echo $this->get_field_id('radio_buttons'); ?>">Otvorite link kao popup(novi tab manjih dimenzija)</label>
                <input class="" id="<?php echo $this->get_field_id('radio_option_2'); ?>" name="<?php echo $this->get_field_name('radio_buttons'); ?>" type="radio" value="sameWindowNewTab" <?php if($radio_buttons === 'sameWindowNewTab'){ echo 'checked="checked"'; } ?> />

            <label for="<?php echo $this->get_field_id('radio_buttons'); ?>">Otvorite link u istom prozoru</label>
            <input class="" id="<?php echo $this->get_field_id('radio_option_3'); ?>" name="<?php echo $this->get_field_name('radio_buttons'); ?>" type="radio" value="sameWindow" <?php if($radio_buttons === 'sameWindow'){ echo 'checked="checked"'; } ?> />
        </div>
        <?php
    }

    /**
     * Front-end display of widget.
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     * @see WP_Widget::widget()
     *
     */

    public function widget($args, $instance)
    {
        // If the link input is not empty
        if (isset($instance['inputField']) && $instance['inputField'] !== '') {
            $openLinkInNewTab = '';
            // Default button color
            $buttonColor = $instance['buttonColor'] ?? 'blueButtonHome';
            // If target href is chosen
            if (isset($instance['radio_buttons'])) {
                switch($instance['radio_buttons']){
                    case 'sameWindow' :
                        break;
                    case 'sameWindowNewTab' :
                        $newTabNewWindow = "window.open('" . $instance['inputField'] ."','popup','width=600,height=600'); return false;";
                        $openLinkInNewTab = 'target="popup" onclick="' . $newTabNewWindow . '"';
                        break;
                    case 'newTab' :
                        $openLinkInNewTab = 'target="_blank"';
                        break;

                    default:
                }
            }
            echo '<div class="buttonContainer">
                <a href="' . $instance['inputField'] . '" class="' . $buttonColor . '" title="' . $instance['buttonText'] . '"' . $openLinkInNewTab . '>
                    ' . $instance['buttonText'] . '
                </a></div>
            ';
        }
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     * @see WP_Widget::update()
     *
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['buttonColor'] = (!empty($new_instance['buttonColor'])) ? $new_instance['buttonColor'] : '';
        $instance['inputField'] = (!empty($new_instance['inputField'])) ? $new_instance['inputField'] : '';
        $instance['buttonText'] = (!empty($new_instance['buttonText'])) ? $new_instance['buttonText'] : '';
        $instance['radio_buttons'] = (!empty($new_instance['radio_buttons'])) ? $new_instance['radio_buttons'] : '';

        return $instance;
    }
}