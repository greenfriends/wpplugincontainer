<?php


namespace GfWpPluginContainer;


class WooOrder
{
    public function init()
    {
        add_action('woocommerce_before_cart_table', [$this,'beforeCartContents']);
    }

    /**
     * Display required details when cancel order is detected.
     */
    public function beforeCartContents()
    {
        if (isset($_GET['cancel_order'])) {
            $orderId = '';
            foreach (explode('&amp;', $_GET['cancel_order']) as $item):
                if (explode('=', $item)[0] == 'order_id'):
                    $orderId = (explode('=', $item)[1]);
                endif;
            endforeach;
            if ($_GET['cancel_order'] && $orderId !== '') {

                //test funkcije
                wp_safe_redirect(wc_get_endpoint_url('my-account'));
            }
        }
    }
}