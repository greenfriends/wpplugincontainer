<?php


namespace GfWpPluginContainer;


class WooOrderAdmin
{
    public function init()
    {
        add_filter('manage_edit-shop_order_columns', [$this, 'newOrderColumn']);
        add_action( 'manage_shop_order_posts_custom_column', [$this,'newOrderColumnPopulate']);
    }

    public function newOrderColumn($columns)
    {
        $newColumns = [];
        foreach ($columns as $colName => $colData){
            $newColumns[$colName] = $colData;

            if ($colName === 'order_status'){
                $newColumns['order_type'] = __('Tip narudžbine', 'gfShopTheme');
            }
        }
        return $newColumns;
    }

    function newOrderColumnPopulate( $column ) {
        global $post;

        if ( 'order_type' === $column ) {

            $order    = wc_get_order( $post->ID );
            $type     = $order->get_meta('gf_type');

            if ($type != 'donation'){
                $type = __('Product', 'gfShopTheme');
            }
            $type = ucfirst($type);
            echo $type;
        }
    }
}