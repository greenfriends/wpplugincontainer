<?php


namespace GfWpPluginContainer;


class WooProductAdmin
{
    /**
     * All actions and filters
     */
    public function init()
    {
        add_action('woocommerce_product_options_general_product_data', [$this, 'addCustomCheckboxToProductAdmin']);
        add_action('woocommerce_process_product_meta', [$this, 'saveCustomCheckbox']);


        add_filter('woocommerce_product_data_tabs', [$this, 'newProductTab']);
        add_filter('woocommerce_product_data_panels', [$this, 'productVideoTab']);
        add_action('woocommerce_process_product_meta', [$this, 'saveVideoTabContent']);

    }

    /**
     *  Creates mastercard only checkbox to product
     */
    public function addCustomCheckboxToProductAdmin()
    {
        global $post;

        $input_checkbox = get_post_meta($post->ID, 'masterCard', true);
        if (empty($input_checkbox)) $input_checkbox = '';
        woocommerce_wp_checkbox(
            array(
                'id' => 'master-checkbox',
                'label' => __('Samo za Mastercard korisnike', 'gfShopTheme'),
                'value' => $input_checkbox,
            )
        );
    }

    /**
     * Saves custom checkbox for mastercard
     * @param $post_id
     */
    public function saveCustomCheckbox($post_id)
    {
        $customCheckbox = isset($_POST['master-checkbox']) ? 'yes' : '';
        update_post_meta($post_id, 'masterCard', $customCheckbox);
    }

    public function newProductTab($tabs)
    {
        $tabs['productVideo'] = [
            'label' => __('Video o proizvodu', 'gfShopTheme'),
            'target' => 'productVideoTab',
        ];
        return $tabs;
    }

    public function productVideoTab()
    {
        global $thepostid;

        ?>
        <div id='productVideoTab' class='panel woocommerce_options_panel'><?php

        ?>
        <div class='options_group'><?php

            woocommerce_wp_text_input([
                'id' => 'youTubeVideo',
                'label' => __('Youtube video kod', 'gfShopTheme'),
                'desc_tip' => 'true',
                'description' => __('Unesite youtube video kod', 'gfShopTheme'),
            ]);
            ?></div>

        </div><?php
    }

    public function saveVideoTabContent($postId)
    {
        if (isset($_POST['youTubeVideo'])) {
            update_post_meta($postId, 'youTubeVideo', $_POST['youTubeVideo']);
        }

    }

}