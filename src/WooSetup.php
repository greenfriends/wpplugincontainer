<?php


namespace GfWpPluginContainer;


class WooSetup
{
    public function initClasses()
    {
        $wooMyAccount = new WooMyAccountPage();
        $wooMyAccount->init();

        $wooHelper = new WooHelper();
        $wooHelper->init();

        $wooSingleProduct = new WooSingleProduct();
        $wooSingleProduct->init();

        $wooProductAdmin = new WooProductAdmin();
        $wooProductAdmin->init();

        $wooOrderAdmin = new WooOrderAdmin();
        $wooOrderAdmin->init();

        $wooOrder = new WooOrder();
        $wooOrder->init();
    }
}