<?php

namespace GfWpPluginContainer\Wp;


/**
 * Class Setup
 *
 * @package GreenFriends\Setup
 */
class Setup
{
    private $config;

    /**
     * Setup constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
        $this->setStylesAndScripts();
        $this->setSupportedFeatures();
        $this->getSidebar();
        $this->setupShortCodes();
        $this->setupNavMenus();
        $this->setupCustomizer();
    }

    /**
     *
     */
    private function setStylesAndScripts()
    {

        foreach ($this->config['styles'] as $handle => $uri) {
            WpEnqueue::addFrontendStyle( $handle, $uri);
        }
        foreach ($this->config['scripts'] as $handle => $uri) {
            if (is_array($uri)) {
                $scriptUri = array_keys($uri)[0];
                WpEnqueue::addFrontendScript($handle, $scriptUri, $uri[$scriptUri]);
            } else {
                WpEnqueue::addFrontendScript($handle, $uri);
            }
        }
        WpEnqueue::actionEnqueueScripts( function () {
            if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
                wp_enqueue_script( 'comment-reply' );
            }
        } );
    }

    /**
     *
     */
    private function setSupportedFeatures()
    {
        $langDomain = array_keys($this->config['languageTextDomain'])[0];
        load_theme_textdomain($langDomain, $this->config['languageTextDomain'][$langDomain]);

        foreach ($this->config['supportedFeatures'] as $index => $feature) {
            if (is_array($feature)) {
                add_theme_support($index, $feature);
            } else {
                add_theme_support($feature);
            }
        }
    }

    /**
     * @return Sidebar
     */
    private function getSidebar()
    {
        return new Sidebar();
    }

    /**
     *
     */
    private function setupShortCodes()
    {
        $shortCodesConfig = require PARENT_THEME_DIR . '/config/shortCodes.php';

        ShortCodes::setupShortCodes(PARENT_THEME_DIR . '/templates', PARENT_THEME_DIR_URI . '/templates', $shortCodesConfig);
    }

    private function setupNavMenus() {
        $menus = $this->config['navMenus'];
        foreach ( $menus as $menuName ) {
            wp_create_nav_menu( $menuName );
        }
    }

    public function setupCustomizer() {

        $customize = $this->config['customize'];

        foreach ( $customize['logos'] as $logo ) {
            add_action( 'customize_register', static function ( $wpCustomize ) use ( $logo ) {
                // add a setting for the site logo
                $wpCustomize->add_setting( $logo['id'] );

                // Add a control to upload the logo
                $wpCustomize->add_control( new \WP_Customize_Image_Control( $wpCustomize, $logo['id'],
                    array(
                        'label'    => $logo['label'],
                        'section'  => $logo['section'],
                        'settings' => $logo['settings'],
                    ) ) );
            } );
        }

        foreach ( $customize['logos'] as $logo ) {
            add_action( 'customize_register', static function ( $wpCustomize ) use ( $logo ) {
                // add a setting for the site logo
                $wpCustomize->add_setting( $logo['id'] );

                // Add a control to upload the logo
                $wpCustomize->add_control( new \WP_Customize_Image_Control( $wpCustomize, $logo['id'],
                    array(
                        'label'    => $logo['label'],
                        'section'  => $logo['section'],
                        'settings' => $logo['settings'],
                    ) ) );
            } );
        }

        foreach ( $customize['logoTitle'] as $logoTitle ) {
            add_action( 'customize_register', static function ( $wpCustomize ) use ( $logoTitle ) {
                // add a setting for the site logo
                $wpCustomize->add_setting( $logoTitle['id'] );

                // Add a control to upload the logo
                $wpCustomize->add_control( new \WP_Customize_Control( $wpCustomize, $logoTitle['id'],
                    array(
                        'label'    => $logoTitle['label'],
                        'section'  => $logoTitle['section'],
                        'settings' => $logoTitle['settings'],
                    ) ) );
            } );
        }




    }
}