<?php
namespace GfWpPluginContainer\Wp;

/**
 * Class WpHelper.
 *
 *
 * @package GfWpPluginContainer\Wp
 */
class WpHelper
{
    public static $template = 'You must have %s plugin active in order for this theme to work properly.';

    public static function noPluginNotice() {
        ob_start();
        sprintf(_e(static::$template, 'gfShopTheme'), CONTAINER_PLUGIN_NAME);
        echo '<div class="notice notice-warning is-dismissible"><p>' . ob_get_clean() . '</p></div>';
    }
}