<?php
//Function is not complete you still need to manually map stuff and there is no file upload
$file = '';
//$file = PLUGIN_DIR . 'import.xlsx';

if (strlen($file) > 0 ){
    /**  Identify the type of $inputFileName  **/
    $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file);
    /**  Create a new Reader of the type that has been identified  **/
    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
    /**  Load $inputFileName to a Spreadsheet Object  **/
    $spreadsheet = $reader->load($file);

    $worksheet = $spreadsheet->setActiveSheetIndexByName('THM lokacije');
    $formatedData = [];
    foreach ($worksheet->getRowIterator() as $row) {
        /** @var \PhpOffice\PhpSpreadsheet\Cell\Cell $cell */
        foreach ($row->getCellIterator() as $cell) {
            if ($cell->getRow() === 1) {
                continue;
            }
            if ($cell->getColumn() === 'A') {
                $name = $cell->getValue();
            }
            if ($cell->getColumn() === 'B') {
                $address = $cell->getValue();
            }
            if ($cell->getColumn() === 'C') {
                $city = $cell->getValue();
            }
            if ($cell->getColumn() === 'D') {
                $zipCode = $cell->getValue();
            }
            if ($cell->getColumn() === 'E') {
                $phone1 = $cell->getValue();
            }
            if ($cell->getColumn() === 'F') {
                $email = $cell->getValue();
            }
            if ($cell->getColumn() === 'G') {
                $latitude = $cell->getValue();
            }
            if ($cell->getColumn() === 'H') {
                $longitude = $cell->getValue();
            }
            if ($cell->getColumn() === 'I') {
                $kayakoId = $cell->getValue();
                insertProdajnoMesto($name, $address, $city, $zipCode, $phone1, $email, $latitude, $longitude, $kayakoId);
            }
        }
    }
}

function insertProdajnoMesto($name, $address, $city, $zipCode, $phone1, $email, $latitude, $longitude, $kayakoId, $phone2 = null, $phone3 = null) {
    $postId = wp_insert_post([
        'post_type' => 'prodajna_mesta',
        'post_title' => $name,
        'post_content' => '',
        'post_status' => 'publish',
    ]);
    if ($postId) {
        // insert post meta
        wp_set_post_terms($postId, 'THM lokacije', 'tip_prodajnog_mesta', '');
        add_post_meta($postId, 'address', $address);
        add_post_meta($postId, 'city', $city);
        add_post_meta($postId, 'zipCode', $zipCode);
        add_post_meta($postId, 'phone1', $phone1);
        add_post_meta($postId, 'phone2', null);
        add_post_meta($postId, 'phone3', null);
        add_post_meta($postId, 'email', $email);
        add_post_meta($postId, 'latitude', $latitude);
        add_post_meta($postId, 'longitude', $longitude);
        add_post_meta($postId, 'kayakoId', $kayakoId);
    } else {
        echo 'Post ' . $name . ' sa adresom: ' . $address . 'nije uspeosno ubacen';
    }
}